using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using WebSocketSharp;
using System;
using System.Threading.Tasks;

using Newtonsoft.Json;


namespace WebSocketSharpNS {

    public class WebSocketSharpConnector : IConnector {

        private Action<Message> _OnMessageReceived;

        public Action<Message> OnMessageReceived {
            get => _OnMessageReceived;
            set => _OnMessageReceived = value;
        }

        private Action<string> _OnAdminMessageReceived;

        public Action<string> OnAdminMessageReceived {
            get => _OnAdminMessageReceived;
            set => _OnAdminMessageReceived = value;
        }

        private WebSocket connection;


        public async Task InitAsync() {

            connection = new WebSocket("ws://localhost:5000/chat");

            connection.OnMessage += (sender, e) => {

                var body = !e.IsPing ? e.Data : "Received a ping.";
                Command command = JsonConvert.DeserializeObject<Command>(body);

                if (command.type.Equals("ReceiveMessage")) {

                    Message msg = JsonConvert.DeserializeObject<Message>(command.data);
                    UnityMainThreadDispatcher.Instance().Enqueue(() => OnMessageReceived?.Invoke(msg));
                } else if (command.type.Equals("AdminMessage")) {

                    string msg = command.data;
                    UnityMainThreadDispatcher.Instance().Enqueue(() => OnAdminMessageReceived?.Invoke(msg));

                } else {
                    UnityEngine.Debug.LogError("Unknown command from websocket: " + command.type);
                }
            };

            await StartConnectionAsync();
        }

        public Task SendMessageAsync(Message message) {
            try {
                string jsonString = JsonConvert.SerializeObject(message);
                connection.Send(jsonString);
            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
            return Task.FromResult(0);
        }

        private Task StartConnectionAsync() {
            try {
                connection.ConnectAsync();
            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
            return Task.FromResult(0);
        }


        public async Task ReconnectAsync() {
            await StartConnectionAsync();
        }


        public Task DisconnectAsync() {
            try {
                connection.CloseAsync();
            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
            return Task.FromResult(0);
        }


        public void DispatchMessageQueue() {
            // there is no message queue in this library, don't need to do anything
            return;
        }

    }
}
