using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Microsoft.AspNetCore.SignalR.Client;

using Newtonsoft.Json;


interface IConnector {
    Task InitAsync();
    Task SendMessageAsync(Message message);
    Task ReconnectAsync();
    Task DisconnectAsync();
    void DispatchMessageQueue();
    Action<Message> OnMessageReceived {
        get;
        set;
    }
    Action<string> OnAdminMessageReceived {
        get;
        set;
    }
}

namespace SignalRNS {

    public class SignalRConnector : IConnector {

        private Action<Message> _OnMessageReceived;

        public Action<Message> OnMessageReceived {
            get => _OnMessageReceived;
            set => _OnMessageReceived = value;
        }

        private Action<string> _OnAdminMessageReceived;

        public Action<string> OnAdminMessageReceived {
            get => _OnAdminMessageReceived;
            set => _OnAdminMessageReceived = value;
        }

        private HubConnection connection;


        public async Task InitAsync() {
            connection = new HubConnectionBuilder()
                    .WithUrl("http://localhost:5000/chat")
                    .Build();

            connection.On<string, string>("ReceiveMessage", (user, message) => {
                OnMessageReceived?.Invoke(new Message {
                    UserName = user,
                    Text = message,
                });
            });

            connection.On<string>("AdminMessage", (message) => {
                OnAdminMessageReceived?.Invoke(message);
            });

            await StartConnectionAsync();
        }


        public async Task SendMessageAsync(Message message) {
            try {
                await connection.InvokeAsync("Send", message.UserName, message.Text);
            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
        }


        private async Task StartConnectionAsync() {
            try {
                await connection.StartAsync();
            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
        }


        public async Task ReconnectAsync() {
            await StartConnectionAsync();
        }


        public async Task DisconnectAsync() {
            try {
                await connection.StopAsync();
            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
        }


        public void DispatchMessageQueue() {
            // there is no message queue in this library, don't need to do anything
            return;
        }
    }
}
